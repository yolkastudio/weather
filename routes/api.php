<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/openweathermap', 'OpenWeathermMapController@current');
Route::get('/geoplugin', 'GeoPluginController@current');
Route::get('/geoplugin/list', 'GeoPluginController@get_list');
