Openweathermap = function () {
    
};

Openweathermap.prototype.currentWeatherData = function (parameters = null, callback = null) 
{
    if (parameters !== null)
    {        
        axios.get('/api/openweathermap',  {
            params: {
                units: parameters.units,
                name_en: parameters.name_en,
            }
	})
            .then(function (response) {
                if (callback instanceof Function)
                {
                    callback(response.data);
                }
            })
            .catch(function (error) {
                console.error(error);
            });
    }
}

