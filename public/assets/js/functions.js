function ucFirst(str) 
{
  // только пустая строка в логическом контексте даст false
  if (!str) return str;

  return str[0].toUpperCase() + str.slice(1);
}

function getFormatWind (wind = null)
{
    if (!wind instanceof Object) return wind;
    
    return Math.floor(wind.speed) + ' ' + this.lang.wind + ', ' + this.lang[this.getCodeAzimut(wind.deg)];
}

/**
 * Возвращает код Азимута
 *  север               north       0° или 360° 
 *  северо-восток	northeast   45°
 *  восток              east        90°
 *  юго-восток          southeast   135°
 *  юг                  south       180°
 *  юго-запад           southwest   225°
 *  запад               west        270°
 *  северо-запад	northwest   315°
 * 
 * @param {Float} deg
 * @returns {String}
 */
function getCodeAzimut (deg = 0)
{
    if (deg === 0 || deg === 360)
        return 'north';
    else if (deg > 0 && deg < 90)
        return 'northeast';
    else if (deg === 90)
        return 'east';
    else if (deg > 90 && deg < 180)
        return 'southeast';
    else if (deg === 180)
        return 'south';
    else if (deg > 180 && deg < 270)
        return 'southwest';
    else if (deg === 270)
        return 'west';
    else 
        return 'northwest';
}


function getFormatChanceOfRain(weather)
{
    if (!weather instanceof Object) return weather;
    
    let chance = 0;
    
    if (weather.id === 800)
        chance = 0;
    else if (weather.id > 800)
        chance = 10;
    else if (weather.id >= 700)
        chance = 10;
    else if (weather.id >= 600)
        chance = 80;
    else if (weather.id >= 500)
        chance = 80;
    else if (weather.id >= 200)
        chance = 80;
    
    return chance + '%';     
}

function setCookie(name, value, options) 
{
    options = options || {};

    var expires = options.expires;

    if (typeof expires == "number" && expires) 
    {
        var d = new Date();
        d.setTime(d.getTime() + expires * 1000);
        expires = options.expires = d;
    }
    if (expires && expires.toUTCString) 
    {
        options.expires = expires.toUTCString();
    }

    value = encodeURIComponent(value);

    var updatedCookie = name + "=" + value;

    for (var propName in options) 
    {
        updatedCookie += "; " + propName;
        var propValue = options[propName];
        if (propValue !== true) 
        {
            updatedCookie += "=" + propValue;
        }
    }

    document.cookie = updatedCookie;
}

function getLocalGeo() 
{
    let vm = this;
        
    navigator.geolocation.getCurrentPosition(function(position) {

        // Текущие координаты.
        var lat = position.coords.latitude + "";
        lat = lat.substr(0,5) * 1;
        var lng = position.coords.longitude + "";
        lng = lng.substr(0,5) * 1;
        
        axios.get('https://geocode-maps.yandex.ru/1.x/?apikey=&geocode='+lng+','+lat+'&format=json&results=1&lang=en_US')
            .then(function (response) {
                let nameCity = response.data.response.GeoObjectCollection.featureMember[0].GeoObject.metaDataProperty.GeocoderMetaData.Address.Components[4].name;
                
                for (let i in vm.geo_cities_list)
                {
                    let city = vm.geo_cities_list[i];
                    
                    if (city.name_en === nameCity)
                    {
                        vm.change_city = city;
                        return true;
                    }
                }
                
            })
            .catch(function (error) {
                console.error(error);
            });
    });
}