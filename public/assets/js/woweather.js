Woweather = function () 
{    
    this.stages = {};
    this.data = {};
    this.init = null;
    this.vm = null;
    
    this.parameters = {
        units: 'metric'
    };
};

/**
 * Запускает приложение
 * 
 * @returns {undefined}
 */
Woweather.prototype.startInit = function () 
{
    if (this.init instanceof Function)
        return this.init(this.data);
    
    return false;
};

/**
 * Устанавливает функцию на запуск приложения 
 * 
 * @param {type} callbakc
 * @returns {undefined}
 */
Woweather.prototype.setEventInit = function (callbakc) 
{
    if (callbakc instanceof  Function)
        this.init = callbakc;
};

Woweather.prototype.setEventUpdate = function (callbakc) 
{
    if (callbakc instanceof  Function)
        this.update = callbakc;
};

/**
 * Проверяет все этапы на загруженность 
 * 
 * @returns {Boolean}
 */
Woweather.prototype.isLoadData = function () 
{
    for (let i in this.stages)
    {
        if (!this.stages[i]) return false;
    }
    
    if (this.vm === null)
        this.vm = this.startInit(this.data);
    
    return true;
};

/**
 * Устанавливаем этапы загрузки приложения
 * 
 * На вход принимает массив, который преобразует в свойства
 * 
 * @param Array stages
 * @returns {Boolean}
 */
Woweather.prototype.setStages = function (stages = null) 
{
    if (Array.isArray(stages))
    {
        for (let i in stages)
            this.stages[stages[i]] = false;
        
        return true;
    }
    return false;
};

/**
 * Удаляет этапы
 * 
 * @returns {undefined}
 */
Woweather.prototype.clearStages = function () 
{
    this.stages = {};
};

/**
 * Устанавливаем загруженные данные приложения
 * 
 * На вход принимает ключ свойства и данные
 * 
 * @param String key
 * @param {type} data
 * @returns {Boolean}
 */
Woweather.prototype.setData = function (key = '', data = undefined) 
{
    if (key.length > 0)
    {
        this.stages[key] = true;
        this.data[key] = data;
        
        return this.isLoadData();
    }
    return false;
};

Woweather.prototype.startSetData = function (callback = null)
{
    if (callback instanceof Function)
    {
        callback();
        
        var timerId = setInterval(callback, 60000);
        
        return timerId;
    }
    return false;
}
