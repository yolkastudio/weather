window.onload = function ()
{ 
    var woweather = new Woweather();
    var openweathermap = new Openweathermap();
    
    woweather.setEventInit(function (data) 
    {
        let vmData = data;
        
        vmData.preloader = true;
        
        vmData.units = woweather.parameters.units;
        vmData.timerId = woweather.timerId;
        vmData.popup_get_cities = false;
        
        vmData.change_city = null;
                 
        var vm = new Vue({
            el: '#woweather',               
            data: vmData,     
            watch: {
                units: function (val) {
                    woweather.parameters.units = val;
                    clearInterval(woweather.timerId);
                    woweather.timerId = woweather.startSetData(woweather.update);
                },
                change_city: function (val) {
                    woweather.data.current_geoplugin = val;
                    woweather.parameters.name_en = val.name_en;
                    clearInterval(woweather.timerId);
                    woweather.timerId = woweather.startSetData(woweather.update);
                    this.setCookie('woweather_city_id', val.id);   
                    this.popup_get_cities = false;
                }
            },
            methods: {
                ucFirst: ucFirst,
                getFormatWind: getFormatWind,
                getCodeAzimut: getCodeAzimut,
                getFormatChanceOfRain: getFormatChanceOfRain,
                setCookie: setCookie,
                getLocalGeo: getLocalGeo,
            },
        });
        
        woweather.clearStages();
        
        Vue.nextTick(function () {
            vmData.preloader = false;
        });
        
        return vm;
    });
    
    woweather.setEventUpdate(function () {
        
        woweather.parameters.name_en = woweather.data.current_geoplugin.name_en;
                
        //set data Weather
        openweathermap.currentWeatherData(woweather.parameters, function (data) {
            data.weather[0].url = '/assets/images/' + data.weather[0].icon + '.png';
            document.body.setAttribute('class', 'bg_' + data.weather[0].icon);
            woweather.setData('current_weather_data', data);
        }); 
        
        console.log(woweather);
    });

    woweather.setStages(['lang', 'current_weather_data', 'current_geoplugin', 'geo_cities_list']);

    //load lang
    axios.get('lang/ru/index.json?' + Math.random())
            .then(function (response) {
                woweather.setData('lang', response.data);
            })
            .catch(function (error) {
                console.error(error);
            });
            
    axios.get('/api/geoplugin/list')
            .then(function (response) {
                woweather.setData('geo_cities_list', response.data)
            })
            .catch(function (error) {
                console.error(error);
            });
          
    axios.get('/api/geoplugin',  {})
            .then(function (response) {
                woweather.setData('current_geoplugin', response.data);
            })
            .catch(function (error) {
                console.error(error);
            })
            .finally(function () {
                //load data  
                woweather.timerId = woweather.startSetData(woweather.update);
            });
          
    
    
    
};




