<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OpenWeathermMap extends Model
{
    const API_URL = 'http://api.openweathermap.org/data/2.5/';
    const APPID = '0aa7006463c5033108ce47d705922d64';
    const LAND = 'ru';

    public static function getCurrent ($q, $units)
    {
        $url = self::API_URL 
            . '/weather/?' 
                . 'q='      . $q 
                . '&APPID=' . self::APPID 
                . '&lang='  . self::LAND 
                . '&units=' . $units;
        
        return self::sendQueryOnApi($url);
    }

    public static function sendQueryOnApi ($url)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); //Устанавливаем параметр, чтобы curl возвращал данные, вместо того, чтобы выводить их в браузер.
        curl_setopt($ch, CURLOPT_URL, $url);

        $data = curl_exec($ch);
        curl_close($ch);

        return $data;
    }
}
