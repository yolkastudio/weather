<?php

namespace App;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class GeoPlugin extends Model
{
    public static function GetCurrentById ($id = null)
    {
        if (!is_null($id) && (int)$id > 0)
        {
            $city = DB::table('geo_plugins')->find($id);
            
            return $city;
        }
    }

    public static function GetCurrent ($ip = null)
    {
        $city = false;
        
        if (!is_null($ip) && strlen($ip) > 0)
        {
            $ip_data = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=".$ip));  

            if($ip_data && $ip_data->geoplugin_city != null)
            {
                $city = DB::table('geo_plugins')->where('name_en', $ip_data->geoplugin_city)->first();

                if (!is_null($city)) return $city;
            }
        } 

        if (!$city)
        {
            $city = DB::table('geo_plugins')->where('name_en', 'Moscow')->first();
            return $city;
        }
    }
    public static function GetList ()
    {
        $cities = DB::table('geo_plugins')->get();
        
        return $cities;
    }
}
