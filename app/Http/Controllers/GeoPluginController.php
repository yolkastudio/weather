<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GeoPluginController extends Controller
{
    public function current(Request $request) 
    {
        $ip = $request->ip();
        
        if (!is_null($request->cookie('woweather_city_id')))
        {            
            $city = \App\GeoPlugin::GetCurrentById($request->cookie('woweather_city_id'));
            
            return json_encode($city);
        }
        else
        {
            $city = \App\GeoPlugin::GetCurrent($ip);
            
            return response(json_encode($city))
                ->cookie('woweather_city_id', $city->id, 9000);
        }
    }
    public function get_list() 
    {
        return json_encode(\App\GeoPlugin::GetList());
    }
}
