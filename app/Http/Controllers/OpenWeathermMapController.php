<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class OpenWeathermMapController extends Controller
{
    public function current(Request $request) 
    {
        $units = $request->input('units');
        $name_en = $request->input('name_en');
        
        $data = \App\OpenWeathermMap::getCurrent($name_en, $units);
        
        return $data;
    }
}
