<!DOCTYPE html>
<html prefix="og: //ogp.me/ns#" lang="ru">
    <head>
        <meta charset="UTF-8">
        
        <title>Погода на каждый день</title>
        
        <meta name="description" 
                content="Онлайн сервис погоды. Данный проект передаёт пагоду согласно вашему местоположению. Выберете совой город и смотрите погоду каждый день" />
        
        <!--Material-->
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900|Material+Icons" rel="stylesheet">
        <!--Vuetify-->
        <link href="https://cdn.jsdelivr.net/npm/vuetify/dist/vuetify.min.css" rel="stylesheet">
        <!--Font Roboto-->
        <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
        
        <link href="https://fonts.googleapis.com/css?family=Pathway+Gothic+One&display=swap" rel="stylesheet">

        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui">
        
        <link rel="apple-touch-icon" sizes="180x180" href="/assets/favicon/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/assets/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/assets/favicon/favicon-16x16.png">
        <link rel="manifest" href="/assets/favicon/site.webmanifest">
        <link rel="mask-icon" href="/assets/favicon/safari-pinned-tab.svg" color="#5bbad5">
        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="theme-color" content="#ffffff">
        
        
        <meta property="og:title" content="Погода на каждый день" />
        <meta property="og:description" 
                content="Онлайн сервис погоды. Данный проект передаёт пагоду согласно вашему местоположению. Выберете совой город и смотрите погоду каждый день" />
        <meta property="og:type" content="website" />
        <meta property="og:image" content="//weather.ylkwb.ru/assets/images/screenshot.jpg" />
        <meta property="og:url" content="//weather.ylkwb.ru/" />
        
        <!--Custom-->
        <link href="assets/css/styles.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div id="woweather">
            <v-app> 
                <div class="preloader" v-if="preloader">
                    <div class="preloader__content">
                        Погода на каждый день
                        <img width="20" src="/assets/images/preloader.gif" alt=""/>
                    </div>
                </div> 
                <v-content>
                    <v-container class="container-full-height">
                        <section class="woweather-header">
                            <v-layout row wrap>
                                <v-flex xs8>
                                    <div class="woweather--title">{{current_geoplugin.name_ru}}</div>
                                    <a class="woweather-link-btn" href="javascript:void(0)" v-on:click="popup_get_cities = !popup_get_cities">{{lang.btn_change_city}}</a>
                                    <a class="woweather-link-btn" href="javascript:void(0)" v-on:click="getLocalGeo"><i class="material-icons woweather-link-btn__icon">navigation</i>{{lang.btn_my_location}}</a>
                                    <div class="woweather-change-city" v-if="popup_get_cities">
                                        <div class="woweather-change-city__content">
                                            <v-autocomplete
                                            v-model="change_city"
                                            v-bind:items="geo_cities_list"
                                            dark
                                            item-text="name_ru"
                                            item-value="id"
                                            v-bind:label="lang.new_change_city"
                                            return-object
                                            ></v-autocomplete>
                                        </div>
                                    </div>
                                </v-flex>
                                <v-flex xs4>
                                    <v-radio-group class="radio-woweather" v-model="units" row>
                                        <v-radio label="C" value="metric"></v-radio>
                                        <v-radio label="F" value="imperial"></v-radio>
                                    </v-radio-group>
                                </v-flex>
                            </v-layout>
                        </section><!--/.woweather-header-->
                        <section class="woweather-main">
                            <v-layout row wrap text-xs-center align-center justify-center fill-height>
                                <v-flex xs12>
                                    <div class="woweather-view">
                                        <span class="woweather_images-block">
                                            <img class="woweather_images" v-bind:src="current_weather_data.weather[0].url" alt=""/>
                                        </span>
                                        <span class="woweather-view__nummber">{{Math.floor(current_weather_data.main.temp)}}</span>
                                        <span class="woweather-view__degree">o</span>
                                        <span class="woweather-view__name">{{ucFirst(current_weather_data.weather[0].description)}}</span>
                                    </div>
                                </v-flex>
                            </v-layout>
                        </section><!--/.woweather-main-->
                        <section class="woweather-footer">
                            <v-layout row wrap text-md-center align-end justify-center fill-height>
                                <v-flex md3 xs6>
                                    <div class="woweather-info">
                                        <span class="woweather-info__title">{{lang.woweather_info_wind}}</span>
                                        <span class="woweather-info__value">{{getFormatWind(current_weather_data.wind)}}</span>
                                    </div>
                                </v-flex>
                                <v-flex md3 xs6>
                                    <div class="woweather-info">
                                        <span class="woweather-info__title">{{lang.woweather_info_pressure}}</span>
                                        <span class="woweather-info__value">{{Math.floor(current_weather_data.main.pressure) + ' ' + lang.pressure}}</span>
                                    </div>
                                </v-flex>
                                <v-flex md3 xs6>
                                    <div class="woweather-info">
                                        <span class="woweather-info__title">{{lang.woweather_info_humidity}}</span>
                                        <span class="woweather-info__value">{{Math.floor(current_weather_data.main.humidity) + '%'}}</span>
                                    </div>
                                </v-flex>
                                <v-flex md3 xs6>
                                    <div class="woweather-info">
                                        <span class="woweather-info__title">{{lang.woweather_info_chance_of_rain}}</span>
                                        <span class="woweather-info__value">{{getFormatChanceOfRain(current_weather_data.weather[0])}}</span>
                                    </div>
                                </v-flex>
                            </v-layout>
                        </section><!--/.woweather-footer-->
                                                
                    </v-container>
                    
                </v-content>
            </v-app>
        </div><!--/#app-->

        <!--Axios-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.19.0/axios.min.js"></script>
        <!--Vue-->
        <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
        <!--Vuetify-->
        <script src="https://cdn.jsdelivr.net/npm/vuetify/dist/vuetify.js"></script>
        <!--Custom--> 
        <script src="assets/js/functions.js" type="text/javascript"></script>
        <script src="assets/js/main.js" type="text/javascript"></script>
        <script src="assets/js/woweather.js" type="text/javascript"></script>
        <script src="assets/js/openweathermap.js" type="text/javascript"></script>
    </body>
</html>